package com.example.demo.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.APIResult;
import com.example.demo.entity.Work;
import com.example.demo.service.WorkService;

@Controller
@RestController
@RequestMapping("/work")

public class WorkController {

	@Autowired
	private WorkService workservice;

	// get list work
	@GetMapping("/getListWorks")
	public ResponseEntity<Map<String, Object>> getListWorks(
			@RequestParam(defaultValue = "0") int page,
		    @RequestParam(defaultValue = "3") int size,
		    @RequestParam(defaultValue = "workName") String sortBy) {
			try {
				List<Work> work = new ArrayList<Work>();
				Pageable paging = PageRequest.of(page, size,Sort.by(sortBy).descending());
				Page<Work> pageTuts;
				pageTuts = workservice.listAll(paging);
				work = pageTuts.getContent();
				Map<String, Object> response = new HashMap<>();
			      response.put("work", work);
			      response.put("currentPage", pageTuts.getNumber());
			      response.put("totalItems", pageTuts.getTotalElements());
			      response.put("totalPages", pageTuts.getTotalPages());
			      return new ResponseEntity<>(response, HttpStatus.OK);
		    } catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}

	// find by workID
	@RequestMapping(value = { "/getwork/{workID}" }, method = RequestMethod.GET)
	public APIResult getWorkByWorkID(@PathVariable("workID") BigInteger workID) {
		APIResult result = new APIResult();
		Work work = workservice.finById(workID);
		result.setData(work);
		result.setSuccess(true);
		return result;
	}

	// create and update work
	@RequestMapping(value = { "/createAndUpdateWork" }, method = RequestMethod.POST)
	public APIResult createAndUpdateWork(@RequestBody Work workRequest) throws Exception {
		return workservice.createAndUpdateWork(workRequest);
	}

	// delete work
	@RequestMapping(value = { "/deleteWork/{workID}" }, method = RequestMethod.GET)
	public APIResult deleteWork(@PathVariable("workID") BigInteger workID) {
		APIResult result = new APIResult();
		try {
			workservice.deleteWork(workID);
			result.setSuccess(true);
			result.setMessage("Success");
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(e.getMessage());
		}
		return result;
	}
}
