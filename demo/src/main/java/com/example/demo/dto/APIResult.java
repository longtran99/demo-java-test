package com.example.demo.dto;

public class APIResult {
	private String errormessage;
	private Object data;
	private boolean success;
	private String message;
	/**
	 * @return get the errormessage
	 */
	public String getErrormessage() {
		return errormessage;
	}
	/**
	 * 
	 * @param the errormessage to set
	 */
	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}
	/**
	 * 
	 * @return get data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * 
	 * @param the data to set 
	 */
	public void setData(Object data) {
		this.data = data;
	}
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
}
