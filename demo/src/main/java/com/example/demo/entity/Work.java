package com.example.demo.entity;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Work {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger workID;

	@Column(name = "Work_Name", length = 50, nullable = false)
	private String workName;

	@Column(name = "Starting_Date")
	private String startingDate;

	@Column(name = "Ending_Date")
	private String ending_Date;

	@Column(name = "Status", length = 10, nullable = false)
	private String status;

	public Work() {
	}


	public Work(BigInteger workID, String workName, String startingDate, String ending_Date, String status) {
		this.workID = workID;
		this.workName = workName;
		this.startingDate = startingDate;
		this.ending_Date = ending_Date;
		this.status = status;
	}

	public BigInteger getWorkID() {
		return workID;
	}

	public void setWorkID(BigInteger workID) {
		this.workID = workID;
	}

	public String getWorkName() {
		return workName;
	}

	public void setWorkName(String workName) {
		this.workName = workName;
	}

	public String getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	public String getEnding_Date() {
		return ending_Date;
	}

	public void setEnding_Date(String ending_Date) {
		this.ending_Date = ending_Date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
