package com.example.demo.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.dto.APIResult;
import com.example.demo.entity.Work;
import com.example.demo.repository.WorkRepository;

@Service
@Transactional
public class WorkService {

	@Autowired
	private WorkRepository repon;

	public Page<Work> listAll(Pageable pageable) {
		return repon.findAll(pageable);

	}

	public Work finById(BigInteger workID) {
		return repon.findById(workID).get();
	}

	public APIResult createAndUpdateWork(Work workRequest) throws Exception {
		APIResult result = new APIResult();
		if (!StringUtils.isEmpty(workRequest.getWorkID())) {
			Work work = this.finById(workRequest.getWorkID());
			
			work = repon.save(workRequest);
			result.setData(work);
			result.setErrormessage("Update Work Success!!!");
			return result;
		}
		repon.save(workRequest);
		result.setData(workRequest);
		result.setErrormessage("Save Work Success!!!");
		return result;
	
	}

	public APIResult deleteWork(BigInteger workID) {
		APIResult result = new APIResult();
		repon.deleteById(workID);
		return result;
	}
	
	public Date getFormatDate(String date, String pattern)  {
    	SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
        if(date!=null){
           try {
        	sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			return sdf.parse(date);
           } catch (ParseException e) {
			System.out.println("ParseException: "+date);
           }
        }
        return null;
	}
    
    public String getFormatDate(Date date, String pattern){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        if(date!=null){
        	sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        	return sdf.format(date);
        }else{
            return "";
        }
    }
}
